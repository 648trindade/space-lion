__author__ = 'rafael'

from enemies import Eyeball
from enemies.shots import EyeShot
from pygame import mask
from math import pi, sin

class LeadEyeball(Eyeball):
    lion, eye, _17dg, _41dg = None, None, None, None

    def __init__(self, pos, sprites, sounds, lion):
        '''
        Inicializa o LeadEyeball
        :param pos: (x,y) -> posicao na tela
        :param screen: {x,y} -> tamanho da tela
        :param clock: int -> frequencia do clock
        :param lion: lion.Lion -> ponteiro ref. ao leao
        :return: Eyeball
        '''
        super().__init__(pos, sprites, sounds)
        self.value = 10
        self.lion = lion
        self._17dg = sin(17*pi/180)
        self._41dg = sin(41.8*pi/180)

    def loadSprites(self):
        self.sprList = self.sprBank.load('enemies','leadEyeball')
        self.sprite = self.sprList[0]
        '''
        self.eye = (image.load("resources/sprites/leadeyeball/leadeyeball0000.png"),
                    image.load("resources/sprites/leadeyeball/leadeyeball0000.png"),
                    image.load("resources/sprites/leadeyeball/leadeyeball0000.png"),
                    image.load("resources/sprites/leadeyeball/leadeyeball0000.png"),
                    image.load("resources/sprites/leadeyeball/leadeyeball0000.png"))
        '''
        self.loadMasks()

    def updateSprite(self):
        super().updateSprite()
        cen = [i for i in map(lambda i, j: i+j, self.sprite.get_rect().center, self.getPos())]
        vet = [i for i in map(lambda i, j: i-j, cen, self.lion.setTarget())]
        if vet[0]>0:
            sinY = vet[1]/((vet[0]**2 + vet[1]**2)**0.5)
            #sinX = vet[0]/c
            if sinY >= self._41dg:   # o >= 60º
                pass #print("mais de 41.8º")
            elif sinY >= self._17dg:  # o >= 30º
                pass #print("mais de 17º")
            elif sinY <= -self._41dg: # o <= -60º
                pass #print("menos de -41.8º")
            elif sinY <= -self._17dg: # o <= -30º
                pass #print("menos de -17º")
            else:       # -30º < o < 30º
                pass #print("entre 17 e -17") # Y 27, 47, X 40


    def shot(self):
        '''
        Executa um tiro.
        Adiciona um tiro na lista de tiros.
        :return: None
        '''
        if self.shotWait is 0:
            self.shots.append( EyeShot((self.spriteSize('w') + self.pos['x'],
                                     self.pos['y'] + self.spriteSize('h')//2),
                                    self.sprBank, self.lion.setTarget()))
            self.shotWait = self.shotTime

