__author__ = 'rafael'

from lion import Ship
from enemies.shots import EyeShot
from pygame import image, mask
from random import randint

class StoneTank(Ship):
    crackOpen, eye = None, None

    def __init__(self, pos, sprites, sounds):
        '''
        Inicializa o StoneEyeball
        :param pos: (x,y) -> posicao na tela
        :param screen: {x,y} -> tamanho da tela
        :param clock: int -> frequencia do clock
        :return: StoneEyeball
        '''
        super().__init__(pos, sprites, sounds)
        self.life = 14
        self.loadSprites()
        self.value = 25
        self.vel = 3
        #self.crackOpen = False
        self.shotWait = (self.screenSize['w'] - self.pos['x'])//self.vel
        self.shotTime = self.clock
        self.moveCount = 0
        self.hitted = False
        self.animTime = self.clock//7.5     # 4 frames tempo de animação (30/7.5 = 4)

    def move(self, lista):
        if not super().move(lista):
            return
        #if self.crackOpen and randint(1,33) is 7:
        if randint(1,33) is 7:
            self.shot()
            self.shotWait = self.clock
        self.moveCount += 1
        if self.moveCount == self.animTime*2:
            self.moveCount = 0

    def shot(self):
        '''
        Executa um tiro.
        Adiciona um tiro na lista de tiros.
        :return: None
        '''
        if self.shotWait is 0:
            self.shots.append( EyeShot([self.spriteSize('w') + self.pos['x'],
                                        self.pos['y'] + self.spriteSize('h')//2],
                                        self.sprBank))
            self.shotWait = self.shotTime

    def loadSprites(self):
        self.sprList = self.sprBank.load('enemies','stoneTank')
        self.sprite = self.sprList['hp7']
        self.loadMasks()

    def loadMasks(self):
        self.mask = mask.from_surface(self.sprite)

    def blit(self,screen,develop): #develop
        super().blit(screen,develop) #develop

    def deathHit(self):
        self.moveCount = 0
        if not (self.life-1)%2:
            self.hitted = True
        return super().deathHit()

    def updateSprite(self):
        if self.life > 0:                                           # se tiver vivo
            if self.hitted:                                         # se foi atingido
                self.sprite = self.sprList['hp'+str(self.life//2)]  # [atualiza] sprite
                if not (self.sprite.__class__ is tuple):            # se sprite não é uma tupla
                    self.mask = mask.from_surface(self.sprite)      # [atualiza] mascara
                self.hitted = False                                 # deixa desatingido
            if self.sprList['hp'+str(self.life//2 + self.life%2)].__class__ is tuple:                   # se é uma tupla
                if not self.moveCount%self.animTime:       # 4 frames pra 30 fps                        # se passaram 4 frames
                    self.sprite = self.sprList['hp'+str(self.life//2 + self.life%2)][self.moveCount//4] # [atualiza] sprite
                    self.mask = mask.from_surface(self.sprite)                                          # [atualiza] mascara


