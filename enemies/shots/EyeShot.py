from lion import Shot

__author__ = 'rafael'

from pygame import Rect, mask, Surface
from math import sqrt, copysign

class EyeShot(Shot):
    small,medium,big,brigth = [None for i in range(4)]

    def __init__(self, pos, sprites, target = None):
        '''
        Metodo construtor do tiro
        Inicializa suas variaveis
        :param pos: posicao inicial do tiro na tela
        :param screenSize: tamanho da tela
        :return: objeto Shot
        '''
        self.screenSize = sprites.dictScreenSize()
        self.active = True
        self.loadSprites(sprites)
        self.calcPos(pos, target)
        self.brigth = -1;
        #self.pos = {'x': pos[0] - 5,
        #            'y': pos[1] - self.sprite.get_height()//2}
        #self.lastPos = {'x': self.pos['x'],
        #                'y': self.pos['y']}

    def loadSprites(self,sprites):
        self.small  = sprites.load('shots','eyeShot','small')
        self.medium = sprites.load('shots','eyeShot','medium')
        self.big    = sprites.load('shots','eyeShot','big')
        self.sprite = {'small': self.small[0],
                       'medium': self.medium[0],
                       'big': self.big[0]}
        self.loadMask()

    def loadMask(self):
        shot = Surface((21,12))
        shot.set_colorkey((0,0,0))
        shot.blit(self.small[0],(0,0))
        shot.blit(self.medium[0],(12,3))
        shot.blit(self.big[0],(18,4.5))
        self.mask = mask.from_surface(shot)

    def calcPos(self,pos,target):
        self.pos = {'big': [pos[0] - 5, pos[1] - self.sprite['big'].get_height()//2]}
        x = pos[0]
        if target is not None:
            diffX = pos[1] != target[1]
            x -= target[0]
        if target is not None and diffX and x >= 0:
            y = target[1] - pos[1]
            c = sqrt(x*x + y*y)
            #self.vel = {'x': 7,
            #            'y': y/(x/7)}
            sinX = x/c
            sinY = y/c
            c2 = self.sprite['big'].get_height()/2 + self.sprite['medium'].get_height()/2
            x2, y2 = sinX*c2, -sinY*c2
            self.pos['medium'] = [pos[0] + x2 - self.sprite['medium'].get_width()//2,
                                  pos[1] + y2 - self.sprite['medium'].get_height()//2]
            c3 = self.sprite['big'].get_height()/2 + self.sprite['medium'].get_height() + self.sprite['small'].get_height()/2
            x3, y3 = sinX*c3, -sinY*c3
            self.pos['small'] = [pos[0] + x3 - self.sprite['small'].get_width()//2,
                                 pos[1] + y3 - self.sprite['small'].get_height()//2]
            self.vel = {'x': 7*sinX,
                        'y': 7*sinY}

        else:
            self.vel = {'x': 7,
                        'y': 0}
            self.pos['medium'] = [self.pos['big'][0] + self.sprite['big'].get_width(),
                                  pos[1] - self.sprite['medium'].get_height()//2]
            self.pos['small'] = [self.pos['medium'][0] + self.sprite['medium'].get_width(),
                                 pos[1] - self.sprite['small'].get_height()//2]

    def move(self):
        '''
        Movimenta o tiro na tela
        :param ship: bool determinando se o tiro foi originario da nave do jogador
        :return: None
        '''
        self.pos['big'][0] -= self.vel['x']
        self.pos['big'][1] += self.vel['y']
        self.pos['medium'][0] -= self.vel['x']
        self.pos['medium'][1] += self.vel['y']
        self.pos['small'][0] -= self.vel['x']
        self.pos['small'][1] += self.vel['y']
        self.updateSprite()

    def offScreen(self, tela):
        return not tela.get_rect().colliderect(self.getRect())

    def getRect(self):
        return Rect((self.pos['big'][0], self.pos['big'][1]),
                    (self.pos['small'][0] + self.sprite['small'].get_width() - self.pos['big'][0],
                     self.sprite['big'].get_height()))


    def updateSprite(self):
        self.brigth += 1
        if self.brigth is 18:
            self.brigth = 0
        if self.brigth%6 is 0:
            self.sprite['small'] = self.small[self.brigth//6]
            self.sprite['medium'] = self.medium[self.brigth//6]
            self.sprite['big'] = self.big[self.brigth//6]

    def blit(self,tela):
        self.move()
        tela.blit(self.sprite['small'],(self.pos['small'][0], int(self.pos['small'][1])))
        tela.blit(self.sprite['medium'],(self.pos['medium'][0], int(self.pos['medium'][1])))
        tela.blit(self.sprite['big'],(self.pos['big'][0], int(self.pos['big'][1])))
        return self.offScreen(tela)

    def getPos(self):
        return self.pos['big']