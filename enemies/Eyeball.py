__author__ = 'rafael'

from random import randint
from enemies.shots import EyeShot
from lion import Ship
from pygame import mask


class Eyeball(Ship):

    def __init__(self, pos, sprites, sounds):
        '''
        Inicializa o Eyeball
        :param pos: [int] -> posicao na tela
        :param sprites: engine.Sprites -> módulo de sprites
        :return: Eyeball
        '''
        super().__init__(pos,sprites, sounds)
        self.value = 5
        self.loadSprites()
        self.moveWait = -1
        self.vel = 5
        self.life = 1
        self.shotWait = (self.screenSize['w'] - self.pos['x'])//self.vel
        self.shotTime = self.clock*3

    def loadSprites(self):
        self.sprList = self.sprBank.load('enemies','eyeball')
        self.sprite = self.sprList[0]
        self.loadMasks()

    def loadMasks(self):
        self.masks = [mask.from_surface(spr) for spr in self.sprList]
        self.mask = self.masks[self.sprList.index(self.sprite)]

    def move(self, lista):
        '''
        Movimenta o eyeball na tela, em 5 pixels a esquerda
        :return: bool -> True se saiu da tela, False senao.
        '''
        if not super().move(lista):
            return
        if randint(1,100) == 7 and not self.offScreen():
            self.shot()
            self.shotWait = self.shotTime

        #return self.offScreen()

    def updateSprite(self):
        self.moveWait += 1
        if self.moveWait == 16:
            self.moveWait = 0
        if not self.moveWait%2:
            self.sprite = self.sprList[self.moveWait//2]
            self.mask = self.masks[self.moveWait//2]

    def shot(self):
        '''
        Executa um tiro.
        Adiciona um tiro na lista de tiros.
        :return: None
        '''
        if self.shotWait is 0:
            self.shots.append( EyeShot([self.spriteSize('w') + self.pos['x'],
                                        self.pos['y'] + self.spriteSize('h')//2],
                                        self.sprBank))
            self.shotWait = self.shotTime

