__author__ = 'rafael'

from pygame import image, mask

class Tunnel():

    def __init__(self, sprites, pos):
        self.screenSize = sprites.dictScreenSize()
        self.clock = sprites.clock
        self.sprite = image.load('resources/images/parkour.png')
        self.mask = mask.from_surface(self.sprite)
        self.pos = pos
        self.vel = 5

    def move(self, blocked):
        if self.pos[0] + self.sprite.get_width() > self.screenSize['w']:
            self.pos[0] -= self.vel
        elif self.pos[0] + self.sprite.get_width() < self.screenSize['w'] and blocked:
            self.pos[0] = self.screenSize['w'] - self.sprite.get_width()
        elif not blocked:
            self.pos[0] -= self.vel

    def blit(self, tela):
        tela.blit(self.sprite,self.pos)

    def getMask(self):
        return self.mask

    def offScreen(self):
        if self.pos[0] <= -self.sprite.get_width():
            return True
        return False

    def getPos(self):
        return self.pos

    def collideList(self, lista):
        offset = [0,0]
        for item in lista:
            offset[0] = int(item.getPos()[0] - self.getPos()[0])
            offset[1] = int(item.getPos()[1] - self.getPos()[1])
            if self.mask.overlap(item.getMask(),offset) is not None:
                return lista.index(item)
        return -1
