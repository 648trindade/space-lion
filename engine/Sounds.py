__author__ = 'rafael'
from pygame.mixer import Sound


class Sounds():
    def __init__(self):
        path = "resources/sounds/"
        self.sounds = { 'lion': {'shot': Sound(path+"lion/SHOT.WAV")}}

    def play(self,obj,action):
        Sound.play(self.sounds[obj][action])
