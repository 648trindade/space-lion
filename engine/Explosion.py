__author__ = 'rafael'

from pygame import gfxdraw

class Explosion():
    def __init__(self):
        self.list = []

    def add(self, pos, raio, cor, vel):
        self.list.append({'pos'     : pos,
                          'radius'  : raio,
                          'color'   : cor,
                          'life'    : 4,
                          'vel'     : vel,
                          'degrade' : tuple([(255 - pig)//4 for pig in cor]),
                          'decrease': (raio - 4)//4})

    def blit(self, tela):
        for explosion in self.list:
            gfxdraw.filled_circle(tela, explosion['pos'][0],explosion['pos'][1],explosion['radius'],explosion['color'])
        self.update()

    def update(self):
        for circle in self.list:
            circle['life'] -= 1
            if circle['life'] == 0:
                self.list.remove(circle)
            else:
                circle['color'] = [circle['color'][i] + circle['degrade'][i] for i in range(3)]
                #circle['color'][3] -= 255//4
                circle['radius'] -= circle['decrease']
                circle['pos'][0] -= circle['vel']
