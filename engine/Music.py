__author__ = "Rafael"

from pygame.mixer import music

class Music():
    '''
    Classe controladora da musica de background no jogo
    '''
    songs = None

    def __init__(self):
        '''
        Método construtor
        '''
        pass

    def start(self, name):
        '''
        Iniciar musica escolhida
        :param name: str -> Nome da Musica
        :return: None
        '''
        if name is 'Blazer':
            music.load("resources/musics/entertheknightblazer.ogg")
        music.play(loops = -1)

    def pause(self):
        music.pause()

    def resume(self):
        music.unpause()

    def stop(self):
        music.stop()
