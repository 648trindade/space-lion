__author__ = 'rafael'

from pygame import freetype

class Score():
    screenSize, clock, score, font10, fonte5 = None, None, None, None, None

    def __init__(self, sprites, record):
        self.screenSize = sprites.screenSize
        self.clock = sprites.clock
        self.score = {'score': 0,
                      'record': record}
        self.font10 = freetype.Font("resources/fonts/game_power.ttf", self.screenSize[1]//10)
        self.font5 = freetype.Font("resources/fonts/game_power.ttf", self.screenSize[1]//5)

    def getScore(self):
        return self.score

    def reset(self):
        if self.score['score'] > self.score['record']:
            self.score['record'] = self.score['score']
        self.score['score'] = 0

    def blit(self, tela):
        fonte = self.font10.render(str(self.score['score']),(255,255,255))[0]
        pos = (self.screenSize[0] - 10 - fonte.get_width(), 10)
        tela.blit(fonte, pos)

    def blitMenu(self, tela):
        record = self.font10.render("record: "+str(self.score['record']),(255,255,255))[0]
        start = self.font5.render("START",(255,255,255))[0]
        posRec = (self.screenSize[0]//2 - record.get_width()//2, self.screenSize[1] - 20 - record.get_height())
        posSta = (self.screenSize[0]//2 - start.get_width()//2, posRec[1] - 20 - start.get_height())
        tela.blit(record, posRec)
        tela.blit(start, posSta)