__author__ = 'rafael'
from pygame import image, transform

class Sprites():
    sprites,clock,screenSize = None, None, None
    def __init__(self, screenSize, clock):
        self.screenSize = screenSize
        self.clock = clock
        path = "resources/sprites/"
        self.sprites = {'lion':{'sprites': {'up45dg'   : image.load(path+"ship/lion_new_20000.png"),
                                            'up15dg'   : image.load(path+"ship/lion_new_20001.png"),
                                            'stopped'  : image.load(path+"ship/lion_new_20002.png"),
                                            'down15dg' : image.load(path+"ship/lion_new_20003.png"),
                                            'down45dg' : image.load(path+"ship/lion_new_20004.png")},
                                'propulsion': tuple([image.load(path+"ship/lion_new_1000"+str(i)+".png") for i in range(5)])},
                        'enemies': {'eyeball': tuple([image.load(path+"eyeball/eyeball000"+str(i)+".png") for i in range(8)]),
                                    'leadEyeball': tuple([image.load(path+"leadeyeball/leadeyeball000"+str(i)+".png") for i in range(8)]),
                                    'stoneTank': {'hp7': image.load(path+"stonetank/tankstone00000.png"),
                                                  'hp6': image.load(path+"stonetank/tankstone10000.png"),
                                                  'hp5': image.load(path+"stonetank/tankstone20000.png"),
                                                  'hp4': tuple([image.load(path+"stonetank/tankstone3000"+str(i)+".png") for i in range(2)]),
                                                  'hp3': tuple([image.load(path+"stonetank/tankstone4000"+str(i)+".png") for i in range(2)]),
                                                  'hp2': tuple([image.load(path+"stonetank/tankstone5000"+str(i)+".png") for i in range(2)]),
                                                  'hp1': tuple([image.load(path+"stonetank/tankstone6000"+str(i)+".png") for i in range(2)])}},
                        'shots': {'shot': image.load(path+"shot/shot1.png"),
                                  'eyeShot': {'small': tuple([image.load(path+"eye-shot/eye_shot_small000"+str(i)+".png") for i in range(3)]),
                                              'medium': tuple([image.load(path+"eye-shot/eye_shot_medium000"+str(i)+".png") for i in range(3)]),
                                              'big': tuple([image.load(path+"eye-shot/eye_shot_big000"+str(i)+".png") for i in range(3)])}}}
        #self.resize((1024,768))

    def load(self, type, set, opt = None):
        if opt is None:
            return self.sprites[type][set]
        else:
            return self.sprites[type][set][opt]

    def dictScreenSize(self):
        return {'w':self.screenSize[0], 'h':self.screenSize[1]}

    def resize(self, screenSize):
        prop = screenSize[1]/600
        for _type,_dict1 in self.sprites.items():   #dict
            for _set,_dict2 in _dict1.items():      #dict, dict
                if _dict2.__class__ is dict:
                    for _opt,item in _dict2.items():
                        if item.__class__ is list:
                            for i in range(len(item)):
                                size = [int(j*prop) for j in item[i].get_size()]
                                item[i] = transform.smoothscale(item[i],size)
                        elif item is not None:
                             size = [int(j*prop) for j in _dict2[_opt].get_size()]
                             _dict2[_opt] = transform.smoothscale(_dict2[_opt],size)
                elif _dict2.__class__ is list:
                    for i in range(len(_dict2)):
                        size = [int(j*prop) for j in _dict2[i].get_size()]
                        _dict2[i] = transform.smoothscale(_dict2[i],size)
                elif _dict2 is not None: #um elemento
                    size = [int(j*prop) for j in _dict1[_set].get_size()]
                    _dict1[_set] = transform.smoothscale(_dict1[_set],size)
