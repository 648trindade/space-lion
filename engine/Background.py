__author__ = 'rafael'

from pygame import image, transform
from random import randint

class Background():
    back, screenSize = None, None

    def __init__(self, sprites):
        self.screenSize = sprites.screenSize
        self.back = transform.scale(image.load("resources/images/background.jpg"),self.screenSize)
        self.stars = []
        self.stars2 = []
        self.createStars()
        self.vel = 7

    def blit(self, screen):
        screen.fill((0,0,0))
        screen.blit(self.back,(0,0))
        self.moveStars()
        self.blitStars(screen)


    def createStars(self):
        for i in range(150):
            self.stars.append([randint(0,self.screenSize[0]),randint(0,self.screenSize[1]),
                               (randint(128,255),randint(128,255),randint(128,255))])
            self.stars2.append([randint(0,self.screenSize[0]),randint(0,self.screenSize[1]),
                                (randint(128,255),randint(128,255),randint(128,255))])

    def blitStars(self, screen):
        for star in self.stars2:
            screen.set_at((int(star[0]),star[1]),star[2])
        for star in self.stars:
            screen.set_at(star[:2],star[2])
            screen.set_at((star[0]+1,star[1]),star[2])
            screen.set_at((star[0],star[1]+1),star[2])
            screen.set_at((star[0]+1,star[1]+1),star[2])


    def moveStars(self):
        leng = len(self.stars)
        leng2 = len(self.stars2)
        for star in self.stars:
            star[0] -= self.vel
            if star[0] < 0:
                self.stars.remove(star)
        for star in self.stars2:
            star[0] -= self.vel/2
            if star[0] < 0:
                self.stars2.remove(star)
        leng -= len(self.stars)
        leng2 -= len(self.stars2)
        for i in range(leng):
            self.stars.append([randint(self.screenSize[0]-self.vel,self.screenSize[0]),randint(0,self.screenSize[1]),
                               (randint(128,255),randint(128,255),randint(128,255))])
        for i in range(leng2):
            self.stars2.append([randint(self.screenSize[0]-self.vel//2,self.screenSize[0]),randint(0,self.screenSize[1]),
                                (randint(128,255),randint(128,255),randint(128,255))])