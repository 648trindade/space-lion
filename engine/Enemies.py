__author__ = 'rafael'

from enemies import Eyeball, LeadEyeball
from obstacles import Tunnel
from engine import Explosion


class Enemies(object):
    shotList, list, screenSize, clock, lion, sprites, sounds = [None for i in range(7)]

    def __init__(self, sprites, sounds, lion):
        self.list = []
        self.screenSize = sprites.dictScreenSize()
        self.clock = sprites.clock
        self.sprites = sprites
        self.shotList = []
        self.lion = lion
        self.wall = None
        self.explosions = Explosion()

    def blit(self, screen, develop = False):
        if self.wall is not None:
            self.wall.blit(screen)
        for enemie in self.list:
            enemie.blit(screen,develop)
        '''####DEVELOP########'''
        if not develop:
            for shot in self.shotList:
                if shot.blit(screen):
                    self.shotList.remove(shot)
            self.explosions.blit(screen)

    def move(self):
        if self.wall is not None:
            self.wall.move(True)
        for enemie in self.list:
            enemie.move(self.list)
        #self.list.remove(enemie)


    def wave1(self):
        pos = {'x': self.screenSize['w'], 'y': 10}
        for i in range(2):
            while True:
                self.list.append(Eyeball([pos['x'],pos['y']], self.sprites, self.sounds))
                pos['x'] += self.list[-1].spriteSize('w') + 10
                pos['y'] += self.list[-1].spriteSize('h')*2 + 20
                if pos['y'] + self.list[-1].spriteSize('h') > self.screenSize['h']:
                    break
            if i is 0:
                pos['x'] = self.list[-1].spriteSize('w')*2 + self.screenSize['w'] + 20
                pos['y'] = self.list[-1].spriteSize('h') + 20
        mid = len(self.list)//4
        pos = self.list[mid].getPos()
        self.list[mid] = LeadEyeball(pos, self.sprites, self.lion, self.sounds)

    def getAll(self):
        return self.list

    def waveEnd(self):
        if len(self.list)>0:
            return False
        else:
            return True

    def finalWave(self):
        self.wall = Tunnel(self.sprites,[1024,0])