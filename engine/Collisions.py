__author__ = 'rafael'

class Collisions():
    lion,enemies, score = None, None, None

    def __init__(self,lion,enemies,score):
        self.lion = lion
        self.enemies = enemies
        self.score = score

    def check(self):
        '''
        Testa a colisao da nave com todos os tiros da tela e com os inimigos
        :return: bool: true se destruido, false se nao
        '''
        #testa colisao com muros
        if self.checkWall():
            return True
        #testa colisao do lion com os tiros sem nave
        if self.checkOrphanShots():
            return True
        for enemie in self.enemies.list:
            if self.checkEnemy(enemie):
                return True
        return False

    def checkWall(self):
        '''
        Testa a colisão com o muro
        :return: bool -> True se lion morrer
        '''
        if self.enemies.wall is None:
            return False
        else:
            if self.lion.collision(self.enemies.wall):
                return True
            colisao = self.enemies.wall.collideList(self.lion.shots)
            if colisao > -1:
                self.enemies.explosions.add(self.lion.shots[colisao].getPunt(),
                                            self.lion.shots[colisao].sprite.get_height()*2,
                                            [120,0,0,255], self.enemies.wall.vel)
                self.lion.shots.pop(colisao)
        return False

    def checkOrphanShots(self):
        '''
        Checa Choque do lion com tiros orfaos
        :return: bool -> True se lion morrer
        '''
        colisao = self.lion.collideList(self.enemies.shotList)
        if colisao > -1:
            self.enemies.shotList.pop(colisao)
            return True
        return False

    def checkEnemy(self, enemie):
        '''
        Testa colisao do lion com um inimigo
        :param enemie: Ship() -> Inimigo
        :return: bool -> True se lion for morto
        '''
        if self.lion.collision(enemie):
            #self.score['score'] += self.enemies.list[self.enemies.list.index(enemie)].getValue()
            self.score['score'] += enemie.getValue()
            self.enemies.shotList += enemie.getShots()
            enemie.destroy()
            self.lion.destroy()
            self.enemies.list.remove(enemie)
            return True
        else:
            #testa colisao do tiro do inimigo com o lion
            colisao = self.lion.collideList(enemie.shots)
            if colisao > -1:
                enemie.shots.pop(colisao)
                return True
            #testa colisao do tiro do lion com o inimigo
            colisao = enemie.collideList(self.lion.shots)
            if colisao > -1:
                self.enemies.explosions.add(self.lion.shots[colisao].getPunt(),
                                            self.lion.shots[colisao].sprite.get_height()*2,
                                            [120,0,0,255], enemie.vel)
                self.lion.shots.pop(colisao)
                if enemie.deathHit():
                    self.score['score'] += enemie.getValue()
                    self.enemies.shotList += enemie.getShots()
                    self.enemies.list.remove(enemie)
            else:
                for shot in self.lion.getShots():
                    for e_shot in enemie.getShots():
                        if shot.collide(e_shot):
                            print("tiros se chocaram!")
        return False