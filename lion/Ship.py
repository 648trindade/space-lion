__author__ = 'rafael'
from pygame import image, Rect

class Ship(object):
    '''
    Classe pai de todas as naves
    '''
    pos, life, vel, sprite, active, mask, clock, sprList, masks, \
    screenSize, shots, shotTime, shotWait, value, sprBank, sound = [None for i in range(16)]

    def __init__(self,pos,sprites, sound):
        self.sound = sound
        self.sprBank = sprites
        self.screenSize = sprites.dictScreenSize()
        self.clock = sprites.clock
        self.pos = {'x': pos[0], 'y': pos[1]}
        self.shots = []

    def move(self,lista):
        self.pos['x'] -= self.vel
        if self.offScreen():
            lista.remove(self)
            return False
        self.updateSprite()
        if self.shotWait > 0:
            self.shotWait -= 1
        return True

    def getPos(self):
        '''
        Converte de dicionario para lista e retorna a posicao da nave na tela
        :return: (x,y): posicao da nave
        '''
        return (self.pos['x'],self.pos['y'])

    def getValue(self):
        return self.value

    def getRect(self):
        '''
        Retorna o retangulo referente ao posicionamento do sprite na tela
        :return: pygame.Rect
        '''
        return Rect(self.getPos(),self.sprite.get_size())

    def offScreen(self):
        '''
        Determina se a nave saiu da tela
        :return: bool -> True se saiu da tela, False senao.
        '''
        if self.pos['x'] + self.spriteSize('w') < 0:
            return True
        else:
            return False


    def spriteSize(self, arg = None):
        '''
        Retorna o tamanho do sprite ativo da nave
        :param arg: argumento referente a dimensao
        :return: Dicionario com o tamanho do sprite, ou o tamanho da dimensao requerida
        '''
        if arg is None:
            return {'w': self.sprite.get_width(),
                    'h': self.sprite.get_height()}
        elif arg == 'h':
            return self.sprite.get_height()
        elif arg == 'w':
            return self.sprite.get_width()

    '''
    def shotsRectList(self):
        \'''
        Retorna uma lista de retangulos dos posicionamentos dos tiros
        :return: [pygame.Rect]
        \'''
        return [shot.getRect() for shot in self.shots]

    def shotsMaskList(self):
        return [shot.getMask() for shot in self.shots]
    '''

    def blit(self,screen, develop = False):
        '''
        Desenha a nave e seus tiros na tela
        :param screen: tela (pygame.display)
        :return: None
        '''
        '''##########DEVELOP##########'''
        if not develop:
            for shot in self.shots:
                if shot.blit(screen):
                    self.shots.remove(shot)
        screen.blit(self.sprite, self.getPos())

    def destroy(self):
        #self.sprite = None
        self.pos['x'] = -100
        self.pos['y'] = -100

    def deathHit(self):
        self.life -= 1
        return self.life == 0

    def getShots(self):
        return self.shots

    def getMask(self):
        return self.mask

    def collideList(self,list):
        offset = [0,0]
        for item in list:
            offset[0] = int(item.getPos()[0] - self.getPos()[0])
            offset[1] = int(item.getPos()[1] - self.getPos()[1])
            if self.mask.overlap(item.getMask(),offset) is not None:
                return list.index(item)
        return -1

    def collision(self, ship):
        offset = (ship.getPos()[0] - self.getPos()[0],
                  ship.getPos()[1] - self.getPos()[1])
        if self.mask.overlap(ship.getMask(),offset) is not None:
            return True
        return False

    def resize(self, prop):
        self.pos['x'] *= prop
        self.pos['y'] *= prop
        for shot in self.shots:
            shot.resize(prop)
        reloadMasks()





