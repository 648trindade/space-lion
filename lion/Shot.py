__author__ = 'rafael'
from pygame import image, Rect, mask, transform

class Shot(object):
    lastPos, pos, active, vel, screenSize, sprite, mask = [None for i in range(7)]

    def __init__(self, pos, sprites):
        '''
        Metodo construtor do tiro
        Inicializa suas variaveis
        :param pos: posicao inicial do tiro na tela
        :param screenSize: tamanho da tela
        :return: objeto Shot
        '''
        self.screenSize = sprites.dictScreenSize()
        self.active = True
        self.vel = 35
        self.loadSprites(sprites)
        self.pos = {'x': pos[0] - 5,
                    'y': pos[1] - self.sprite.get_height()//2}
        self.lastPos = {'x': self.pos['x'],
                        'y': self.pos['y']}

    def loadSprites(self, sprites):
        self.sprite = sprites.load('shots','shot')
        self.loadMask()

    def loadMask(self):
        _mask = transform.scale(self.sprite,(self.sprite.get_width()+self.vel,self.sprite.get_height()))
        _mask.blit(self.sprite,(self.vel,0))
        self.mask = mask.from_surface(_mask)

    def move(self, ship):
        '''
        Movimenta o tiro na tela
        :param ship: bool determinando se o tiro foi originario da nave do jogador
        :return: None
        '''
        self.lastPos['x'] = self.pos['x'] + self.sprite.get_width()
        self.pos['x'] += self.vel



    def offScreen(self):
        '''
        Determina se o tiro esta fora da tela
        :return: True se fora, False se dentro
        '''
        if self.pos['x'] < 0 or self.pos['x'] >= self.screenSize['w']:
            return True
        elif self.pos['y'] < 0 or self.pos['y'] >= self.screenSize['h']:
            return True
        else:
            return False


    def destroyShot(self):
        '''
        Destroi o tiro
        :return: None
        '''
        self.pos['x'] = -100
        self.pos['y'] = -100
        self.active = False
        self.vel = 0

    def getRect(self):
        #return Rect([self.pos['x'],self.pos['y']],
        #            self.sprite.get_size())
        return Rect((self.lastPos['x'],self.lastPos['y']),
                    (self.vel + self.sprite.get_width(), self.sprite.get_height()))

    def getMask(self):
        '''
        Retorna a máscara de bits opacos do sprite
        :return: pygame.mask
        '''
        return self.mask

    def getPos(self):
        '''
        Retorna a posição relativa ao canto superior esquerdo do sprite
        :return: (x,y)
        '''
        return (self.lastPos['x'],self.lastPos['y'])
        #return (self.pos['x'],self.pos['y'])

    def blit(self,tela):
        '''
        Renderiza o tiro imprimindo-o na tela. Retorna True se o tiro estiver fora da tela.
        :param tela: pygame.Surface -> tela do jogo
        :return: bool
        '''
        self.move(True)
        tela.blit(self.sprite,self.getPos())
        return self.offScreen()

    def collide(self, shot):
        '''
        Testa se a mascara de bits do tiro tem intersecção com a do outro tiro
        :param shot: lion.Shot ou herdados
        :return: bool
        '''
        offset = (int(shot.getPos()[0] - self.getPos()[0]),
                  int(shot.getPos()[1] - self.getPos()[1]))
        if self.mask.overlap(shot.getMask(),offset) is not None:
            return True
        return False

    def getPunt(self):
        return [self.pos['x'] + self.sprite.get_width(), self.pos['y'] + self.sprite.get_height()//2]