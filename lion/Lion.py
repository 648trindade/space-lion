__author__ = 'rafael'

from lion import Shot, Ship
from pygame import mask

class Lion(Ship):
    '''
    Nave do jogador
    '''
    inclination, propulsion = None, None

    def __init__(self, sprites, sound):
        '''
        Inicializa nave e seus atributos.
        :param sprites: engine.Sprites -> módulo de sprites
        :return: Lion
        '''
        super().__init__((0,0),sprites, sound)
        self.propulsion = {'count': -1,
                           'sprite': None,
                           'sprList': None,
                           'pos': None}
        self.loadSprites()
        self.inclination= 0
        self.pos        = {'x': 10,
                            'y': self.screenSize['h']//2 - self.spriteSize()['h']//2}
        self.propulsion['pos'] = [self.pos['x'] - 4 ,
                                  self.pos['y'] - ((self.propulsion['sprite'].get_height() - self.spriteSize('h'))//2)]
        self.vel        = 7
        self.shotWait   = 2
        self.shotTime   = self.clock//2
        self.fw_t = 0

    def loadSprites(self):
        '''
        Carrega arquivos de sprite
        :return: None
        '''
        self.sprList = self.sprBank.load('lion','sprites')
        self.sprite = self.sprList['stopped']
        self.propulsion['sprList'] = self.sprBank.load('lion','propulsion')
        self.propulsion['sprite'] = self.propulsion['sprList'][0]
        self.loadMasks()

    def loadMasks(self):
        '''
        Carrega Mascaras
        :return:
        '''
        self.masks = {'up45dg'  : mask.from_surface(self.sprList['up45dg']),
                      'up15dg'  : mask.from_surface(self.sprList['up15dg']),
                      'stopped'  : mask.from_surface(self.sprList['stopped']),
                      'down15dg'  : mask.from_surface(self.sprList['down15dg']),
                      'down45dg'  : mask.from_surface(self.sprList['down45dg'])}
        for name,sprite in self.sprList.items():
            if self.sprite is sprite:
                self.mask = self.masks[name]
                break

    def move(self, Keys):
        '''
        Movimenta a nave conforme teclas direcionais pressionadas
        :param Keys: [bool]: teclas direcionais.
        :return: None
        '''

        #MOVIMENTA VERTICALMENTE
        if Keys['down']:
            self.pos['y']       += self.vel
            self.inclination    -= 1
        elif Keys['up']:
            self.pos['y']       -= self.vel
            self.inclination    +=1
        else:
            if self.inclination   >= self.clock//3:
                self.inclination  -= self.clock//3
            elif self.inclination <= -self.clock//3:
                self.inclination  += self.clock//3
            else:
                self.inclination   = 0
        if self.inclination > 10:
            self.inclination = 10
        elif self.inclination < -10:
            self.inclination = -10

        #ATUALIZA SPRITE
        self.updateSprite()

        #ATUALIZA TEMPO DE ESPERA DO TIRO:
        if self.shotWait > 0:
            self.shotWait -= 1

        #MOVIMENTA HORIZONTALMENTE
        if Keys['left']:
            self.pos['x'] -= self.vel
        elif Keys['right']:
            self.pos['x'] += self.vel

        #CORRIGE POSICAO TELA
        if self.offScreen():
            ## Corrigindo incorrecoes no posicionamento
            if self.pos['x'] < 0:
                #fora da tela horizontalmente a esquerda
                self.pos['x'] = 0
            elif self.pos['x'] + self.spriteSize('w') >= self.screenSize['w']:
                #fora da tela horizontalmente a direita
                self.pos['x'] = self.screenSize['w'] - self.spriteSize('w') - 1

            if self.pos['y'] < 0:
                #fora da tela verticalmente acima
                self.pos['y'] = 0
            elif self.pos['y']+self.spriteSize('h') >= self.screenSize['h']:
                #fora da tela verticalmente abaixo
                self.pos['y'] = self.screenSize['h'] - self.spriteSize('h') - 1

        self.propulsion['pos'][0], self.propulsion['pos'][1] = self.pos['x']-6, self.pos['y']-6

        if Keys['fire']:
            self.shot()

    def offScreen(self):
        '''
        Determina se o lion esta fora da tela
        :return: bool: True se fora, False se dentro
        '''
        if self.pos['x'] < 0 or self.pos['x'] + self.spriteSize('w') >= self.screenSize['w']:
            return True
        elif self.pos['y'] < 0 or self.pos['y'] + self.spriteSize('h') >= self.screenSize['h']:
            return True
        else:
            return False

    def updateSprite(self):
        '''
        Atualiza sprite conforme inclinacao da nave
        :return: None
        '''
        if self.inclination <= self.clock//15 and self.inclination >= -self.clock//15:
            opt = 'stopped'
        elif self.inclination < -self.clock//15 and self.inclination > -self.clock//5:
            opt = 'down15dg'
        elif self.inclination <= -self.clock//5:
            opt = 'down45dg'
        elif self.inclination > self.clock//15 and self.inclination < self.clock//5:
            opt = 'up15dg'
        else:
            opt = 'up45dg'
        self.sprite = self.sprList[opt]
        self.mask = self.masks[opt]

        self.propulsion['count'] += 1
        if self.propulsion['count'] >= 10:
            self.propulsion['count'] = 0
        self.propulsion['sprite'] = self.propulsion['sprList'][self.propulsion['count']//2]

    def shot(self):
        '''
        Executa um tiro.
        Adiciona um tiro na lista de tiros.
        :return: None
        '''
        '''
        if self.shotWait is 0:
            self.shots.append( Shot([self.spriteSize('w') + self.pos['x'],
                                     self.pos['y'] + self.spriteSize('h')//2],
                                    self.screenSize))
            self.shotWait = self.shotTime
        '''
        if len(self.shots) < 3 and self.shotWait is 0:
            self.sound.play('lion','shot')
            self.shots.append( Shot([self.spriteSize('w') + self.pos['x'],
                                     self.pos['y'] + self.spriteSize('h')//2],
                                    self.sprBank))
            self.shotWait = 2

    def blit(self,screen):
        '''
        Renderiza o leao na tela
        :param screen: pygame.Surface
        :return: None
        '''
        super().blit(screen)
        screen.blit(self.propulsion['sprite'],self.propulsion['pos'])

    def setTarget(self):
        '''
        Retorn coordenadas do meio do sprite do leao
        :return: (x,y)
        '''
        return self.getRect().center

    def resize(self, prop):
        super().resize(prop)
        self.propulsion['x'] = self.pos['y'] - 4*prop
        self.propulsion['y'] = self.pos['y'] - ((self.propulsion['sprite'].get_height() - self.spriteSize('h'))//2)
