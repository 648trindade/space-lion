from cx_Freeze import setup, Executable
import sys, os

includefiles=["resources"]
if sys.platform == "win32":
    exe = Executable(
        script="__init__.py",
        #icon="sbb-tree.ico,"
        base="Win32GUI",
        appendScriptToExe=True,
        appendScriptToLibrary=False
        )
else:
    exe = Executable(
        script="__init__.py",
        appendScriptToExe=True,
        appendScriptToLibrary=False
        )
    includefiles.append("run")
buildOptions = dict(create_shared_zip=False, include_files=includefiles)
setup(
    name = "Lion",
    version = "0.1.x",
    description = "Shooter",
    author = "Rafael Trindade, Henrique Pereira",
    options=dict(build_exe=buildOptions),
    executables = [exe]
    )

if sys.platform != "win32":
    print("[...] Movendo pasta destino do build")
    if not os.system("mv build/exe* deb-package/package/usr/games/space_lion && rm -r build"):
        print("[...] Criando script de execucao")
        print("[...] Renomeando executavel")
        if not os.system("mv deb-package/package/usr/games/space_lion/__init__ deb-package/package/usr/games/space_lion/spacelion"):
            print("[...] Alterando permissoes da pasta-alvo do pacote")
            if not os.system("chmod -R 0755 deb-package/package"):
                print("[...] Construindo o pacote")
                if not os.system("dpkg-deb -b deb-package/package deb-package/"):
                    #print("[...] Removendo vestigios do build")
                    #os.system("rm -R deb-package/package/usr/games/space-lion && chmod 0777 -R deb-package/package")
                    print("\nAplicação criada!")
                    quit()
    print("\nAlguma coisa não deu certo...")
'''
python-3.2 setup.py build
'''
