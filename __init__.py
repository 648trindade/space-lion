__author__ = 'rafael'
from engine import *
from lion import Lion
from enemies import Eyeball, LeadEyeball, StoneTank
import pygame, os
from pygame.gfxdraw import box
import time

def resetaNaves():
    global leao, inimigos, clock, colisoes, score, sprites
    score.reset()
    leao = Lion(sprites, sons)
    inimigos = Enemies(sprites, sons, leao)
    colisoes = Collisions(leao,inimigos, score.getScore())

def carregaJoystick():
    global joy
    pygame.joystick.init()
    if pygame.joystick.get_count > 0:
        joy = pygame.joystick.Joystick(0)

def plota():
    global leao, tela, timer, keys, clock, back, colisoes, score
    back.blit(tela)
    leao.move(keys)
    inimigos.move()
    r = colisoes.check()
    leao.blit(tela)
    inimigos.blit(tela)
    score.blit(tela)
    eventos()
    pygame.display.flip()
    timer.tick(clock)
    return r

def eventos():
    global leao, keys, wasd, paused, tela, inimigos, dimensoes, clock, sprites, sons #4 DEVELOP
    for evento in pygame.event.get():
        if (evento.type is pygame.QUIT) or \
                (pygame.key.get_pressed()[pygame.K_LALT] and pygame.key.get_pressed()[pygame.K_F4]):
            #pygame.image.save(tela,"shot.jpg")
            pygame.quit()
            quit()
        elif evento.type in [pygame.KEYDOWN, pygame.JOYBUTTONDOWN]:
            # TESTA PAUSE
            if joy is not None:
                jPause = joy.get_button(11)
                jSelect = joy.get_button(8)
            else:
                jPause, jSelect = False, False
            if pygame.key.get_pressed()[pygame.K_ESCAPE] or jPause:
                paused = not paused
                if paused:
                    pause()
            elif pygame.key.get_pressed()[pygame.K_f] or jSelect:
                mexeTela()
        elif evento.type is pygame.MOUSEBUTTONDOWN:
            '''############DEVELOP##############'''
            if paused:
                if pygame.key.get_pressed()[pygame.K_1] or pygame.key.get_pressed()[pygame.K_2]:
                    inimigos.list.append(Eyeball(pygame.mouse.get_pos(), sprites, sons))
                    op = 'eyeball'
                    if pygame.key.get_pressed()[pygame.K_1]:
                        inimigos.list[-1].vel = 0
                        op += ' parado'
                elif pygame.key.get_pressed()[pygame.K_3] or pygame.key.get_pressed()[pygame.K_4]:
                    inimigos.list.append(LeadEyeball(pygame.mouse.get_pos(), sprites, sons, leao))
                    op = 'leadeyeball'
                    if pygame.key.get_pressed()[pygame.K_3]:
                        inimigos.list[-1].vel = 0
                        op += ' parado'
                elif pygame.key.get_pressed()[pygame.K_5] or pygame.key.get_pressed()[pygame.K_6]:
                    inimigos.list.append(StoneTank(pygame.mouse.get_pos(), sprites, sons))
                    op = 'stonetank'
                    if pygame.key.get_pressed()[pygame.K_5]:
                        inimigos.list[-1].vel = 0
                        op += ' parado'
                if sum(pygame.key.get_pressed()[pygame.K_1:pygame.K_7]) > 0:
                    print (pygame.mouse.get_pos(),op)
            '''############END#################'''

    Tkeys = pygame.key.get_pressed()

    if wasd:
        keys['up'] = Tkeys[pygame.K_w]
        keys['down'] = Tkeys[pygame.K_s]
        keys['right'] = Tkeys[pygame.K_d]
        keys['left'] = Tkeys[pygame.K_a]
        keys['fire'] = Tkeys[pygame.K_COMMA]
    else:
        keys['up'],keys['down'],keys['right'],keys['left'] = Tkeys[pygame.K_UP:pygame.K_LEFT+1]
        keys['fire'] = Tkeys[pygame.K_c]
    keys['start'] = Tkeys[pygame.K_RETURN]
    keys['pause'] = Tkeys[pygame.K_ESCAPE]

    if joy is not None:
        keys['up'] = keys['up'] or (joy.get_hat(0)[1] is 1) or (joy.get_axis(1) < -0.1)
        keys['down'] = keys['down'] or (joy.get_hat(0)[1] is -1) or (joy.get_axis(1) > 0.1)
        keys['right'] = keys['right'] or (joy.get_hat(0)[0] is 1) or (joy.get_axis(0) < -0.1)
        keys['left'] = keys['left'] or (joy.get_hat(0)[0] is -1) or (joy.get_axis(0) > 0.1)
        keys['fire'] = keys['fire'] or joy.get_button(1)
        keys['pause'] = keys['pause'] or joy.get_button(11)

def pause():
    global paused, tela, inimigos #DEVELOP
    temp = tela.copy()
    box(temp,((0,0),dimensoes), pygame.Color(0,0,0,100))
    while paused:
        tela.blit(temp,(0,0))
        eventos()
        '''###############DEVELOP#############'''
        inimigos.blit(tela,True)
        pygame.display.flip()
        timer.tick(clock)

def mexeTela():
    global tela, fullscreen, dimensoes
    fullscreen = not fullscreen
    if fullscreen:
        tela = pygame.display.set_mode(dimensoes, pygame.FULLSCREEN | pygame.HWACCEL | pygame.DOUBLEBUF)
    else:
        tela = pygame.display.set_mode(dimensoes, pygame.NOFRAME)
    pygame.display.flip()

def menu():
    global keys, tela, score
    tela.fill([155,0,0])
    score.blitMenu(tela)
    pygame.display.flip()
    end = False
    while not end:
        #plota menu
        eventos()
        end = keys['start']
    return True

'''
#######################################################################################################################
'''
def jogo():
    global dimensoes, tela, leao, keys, inimigos
    resetaNaves()
    if menu():
        music.start("Blazer")
        while True:
            #inimigos.wave1() DEVELOP
            inimigos.finalWave()
            #while not inimigos.waveEnd(): DEVELOP
            while True:
                if plota(): return False

'''
#######################################################################################################################
'''

def load():
    global dimensoes, sons, clock, paused, wasd, timer, music, keys, back, tela, fullscreen, score, sprites
    os.environ['SDL_VIDEO_CENTERED'] = 'true'
    dimensoes = [1024,600]
    clock = 30
    paused, wasd, fullscreen = False, False, False
    pygame.init()
    #dimensoes = pygame.display.list_modes()[0]
    timer = pygame.time.Clock()
    music = Music()
    sons = Sounds()
    keys = {'up': 0,
            'down': 0,
            'right': 0,
            'left': 0,
            'fire': 0,
            'start': 0,
            'pause': 0}
    #pygame.display.set_icon( pygame.image.load("resources/images/icon.png") )
    pygame.display.set_caption("Spaceship Battle: Crossing Enemies Frontiers")
    pygame.display.set_icon(pygame.image.load("resources/images/icon.png"))
    print("Resoluções disponíveis",pygame.display.list_modes())
    tela = pygame.display.set_mode(dimensoes, pygame.DOUBLEBUF | pygame.NOFRAME)#,  | pygame.FULLSCREEN | pygame.DOUBLEBUF)
    sprites = Sprites(dimensoes,clock)
    back = Background(sprites)
    score = Score(sprites,0)

#MAIN
dimensoes, clock, sons, paused, wasd, timer, music, keys, back, tela, colisoes, leao, inimigos, joy, \
    fullscreen, score, sprites = [None for i in range(17)]
load()
while not jogo():
    music.stop()
